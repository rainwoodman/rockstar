#ifndef _IO_MP_GADGET_H_
#define _IO_MP_GADGET_H_
#include <stdint.h>
#include "../particle.h"

void load_particles_mp_gadget(char *filename, struct particle **p, int64_t *num_p);

#endif /* _IO_MP_GADGET_H_ */
