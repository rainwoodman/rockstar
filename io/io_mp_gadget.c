#include <bigfile.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "io_mp_gadget.h"
#include "../check_syscalls.h"
#include "../config_vars.h"
#include "../config.h"
#include "../particle.h"
#include "../universal_constants.h"

void load_big_header(BigFile *bf, double *OmegaLambda, double *Omega0,  double *h, double *scale, double * mass, double *length, int64_t *tot_part){
    
    BigBlock bb = {0};
    uint64_t nt[MP_GADGET_N_PARTICLE_TYPES];
    double m[MP_GADGET_N_PARTICLE_TYPES];

    if(0 != big_file_open_block(bf, &bb, "header")) {
        fprintf(stderr, "[Error] Failed to open header block : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
    
   
   if(0 != big_block_get_attr(&bb, "Time", scale, "<f8", 1)) {
        fprintf(stderr, "[Error] Failed to read header field Time : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
    
    if(0 != big_block_get_attr(&bb, "MassTable", m, "<f8", MP_GADGET_N_PARTICLE_TYPES)) {
        fprintf(stderr, "[Error] Failed to read header field MassTable : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
    *mass = m[MP_GADGET_HALO_PARTICLE_TYPE];
    
    if(0 != big_block_get_attr(&bb, "BoxSize", length, "<f8", 1)) {
        fprintf(stderr, "[Error] Failed to read header field BoxSize : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }

    if(0 != big_block_get_attr(&bb, "HubbleParam", h, "<f8", 1)) {
        fprintf(stderr, "[Error] Failed to read header field HubbleParam : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
        
    if(0 != big_block_get_attr(&bb, "Omega0", Omega0, "<f8", 1)) {
        fprintf(stderr, "[Error] Failed to read header field Omega0 : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
        
    if(0 != big_block_get_attr(&bb, "OmegaLambda", OmegaLambda, "<f8", 1)) {
        fprintf(stderr, "[Error] Failed to read header field OmegaLambda : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
    
    if(0 != big_block_get_attr(&bb, MP_GADGET_TOTAL_PARTICLES_KEY, nt, "<i8", MP_GADGET_N_PARTICLE_TYPES)) {
        fprintf(stderr, "[Error] Failed to read header field TotNumPart : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }   
    *tot_part = nt[MP_GADGET_HALO_PARTICLE_TYPE];
    
    if(0 != big_block_close(&bb)){
        fprintf(stderr, "[Error] Failed to close header block : %s\n", big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
}

void load_big_array(BigFile *bf, const char * blockname, int blockId, BigArray * arr, const char *dtype){
    
    BigBlock bb = {0};

    if(0 != big_file_open_block(bf, &bb, blockname)) {
        fprintf(stderr, "[Error] Failed to open particle block %s : %s\n", blockname, big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
    
    // If the block Id is larger than the number of blocks in the bigfile, just ignore
    if(blockId >= bb.Nfile){
        
        return;
    }
    
    ptrdiff_t start = bb.foffset[blockId];
    ptrdiff_t size  = bb.fsize[blockId];
    if(0 != big_block_read_simple(&bb, start, size, arr, dtype)){
        fprintf(stderr, "[Error] Could not read particle block %s.%d : %s\n", blockname, blockId, big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
    
    
    if(0 != big_block_close(&bb)){
        fprintf(stderr, "[Error] Could not close block %s.%d : %s\n", blockname, blockId, big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
}

void load_particles_mp_gadget(char *filename, struct particle **p, int64_t *num_p){
    
    BigFile bf = {0};
    
    // Extract the block number from the filename
    int blockId = 0;
    char * basename = (char *) malloc(strlen(filename) + 1);
    
    char *pch = strrchr(filename, '.');
    if(pch == NULL){
        fprintf(stderr, "[Error] Expected snapshot filenames with format <snapshot name and number>.<block>\n");
        exit(EXIT_FAILURE);
    }
    blockId = atoi(pch+1);
    strncpy(basename, filename, pch - filename);
    basename[pch - filename] = '\0';
    
    // Open bigfile
    if(0 != big_file_open(&bf, basename)) {
        fprintf(stderr, "[Error] Failed to open: %s : %s\n", basename, big_file_get_error_message());
        exit(EXIT_FAILURE);
    }
    
    // Read header
    double OmegaLambda, Omega0, h, scale, mass, length;
    int64_t tot_part;
    load_big_header(&bf, &OmegaLambda, &Omega0, &h, &scale, &mass, &length, &tot_part);
    Ol = Omega0;
    Om = OmegaLambda;
    h0 = h;
    SCALE_NOW = scale;
    PARTICLE_MASS = mass * MP_GADGET_MASS_CONVERSION;
    BOX_SIZE = length * MP_GADGET_LENGTH_CONVERSION;
    TOTAL_PARTICLES = tot_part;
    
    AVG_PARTICLE_SPACING = cbrt(PARTICLE_MASS / (Om*CRITICAL_DENSITY));

    if(RESCALE_PARTICLE_MASS)
        PARTICLE_MASS = Om*CRITICAL_DENSITY * pow(BOX_SIZE, 3) / TOTAL_PARTICLES;
 
    printf("MP-GADGET: filename:       %s\n", basename);
    printf("MP-GADGET: box size:       %g Mpc/h\n", BOX_SIZE);
    printf("MP-GADGET: h0:             %g\n", h0);
    printf("MP-GADGET: scale factor:   %g\n", SCALE_NOW);
    printf("MP-GADGET: Total DM Part:  %" PRIu64 "\n", TOTAL_PARTICLES);
    printf("MP-GADGET: DM Part Mass:   %g Msun/h\n", PARTICLE_MASS);
    printf("MP-GADGET: avgPartSpacing: %g Mpc/h\n\n", AVG_PARTICLE_SPACING);
    
    // Open halo particle blocks
    char blockname_pos[256];
    char blockname_vel[256];
    char blockname_id[256];
    
    
    sprintf(blockname_pos, "%ld/Position%s", MP_GADGET_HALO_PARTICLE_TYPE, MP_GADGET_BLOCK_SUFFIX);
    sprintf(blockname_vel, "%ld/Velocity%s", MP_GADGET_HALO_PARTICLE_TYPE, MP_GADGET_BLOCK_SUFFIX);
    sprintf(blockname_id , "%ld/ID%s", MP_GADGET_HALO_PARTICLE_TYPE, MP_GADGET_BLOCK_SUFFIX);
    
    // Read in data from the blocks
    BigArray arr_pos = {0};
    BigArray arr_vel = {0};
    BigArray arr_id  = {0};
    
    load_big_array(&bf, blockname_pos, blockId, &arr_pos, "<f4");
    load_big_array(&bf, blockname_vel, blockId, &arr_vel, "<f4");
    load_big_array(&bf, blockname_id,  blockId, &arr_id,  "<i8");
    
    int64_t halo_particles = arr_pos.dims[0];
    
    *p = (struct particle *)check_realloc(*p, ((*num_p)+halo_particles)*sizeof(struct particle), "Allocating particles.");

    int64_t i=0;
    int64_t j=0;
    for(i=0; i < halo_particles; i++){
        for(j=0; j < 3 ; j++){
            (*p)[i + (*num_p)].pos[j]   = ((float *) arr_pos.data)[i*3 + j] *  MP_GADGET_LENGTH_CONVERSION;
            (*p)[i + (*num_p)].pos[j+3] = ((float *) arr_vel.data)[i*3 + j] * MP_GADGET_VELOCITY_CONVERSION / SCALE_NOW;
        }
        (*p)[i + (*num_p)].id = ((int64_t *) arr_id.data)[j];
    }

    *num_p += halo_particles;

    // Free arrays
    if(0 != big_file_close(&bf)) {
        fprintf(stderr, "[Error] Failed to close: %s : %s\n", basename, big_file_get_error_message());
        exit(EXIT_FAILURE);
    }

    free(basename);
    free(arr_vel.data);
    free(arr_pos.data);
    free(arr_id.data);
}
